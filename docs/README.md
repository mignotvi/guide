# Guide — `API 0057` — E22

## Organisation

- **Commmunication : [Mattermost, _API0057 E22 - Applied Data Science_](https://team.picasoft.net/signup_user_complete/?id=x8mecu3kjpdbudpypuwheiwu7o)**
- Modalité : Présentiel
- Lieu : BF A321
- Horaires :
  - 9h début des hostilités
  - 13h -> 14h30 Pause déjeuner pour faire le plein d'énergie
  - 18h30 fin théorique
- Amener son 💻
- [Une VM individuelle chez Scaleway 🔥](./scaleway-vm/README.md)

## Encadrement

- Jean-Benoist Léger, Enseignant-Chercher [@UTC](https://www.utc.fr/)
- Florent Chehab, GI UTC FDD 2019 (Full-Stack) Data-Scientist [@Qomon](https://qomon.com/) / [Linkedin](https://www.linkedin.com/in/flochehab/)

_Avec le soutien de Thibaud Le Graverend GI UTC FDD 2022 [@Lum::invent](https://luminvent.com) / [Linkedin](https://www.linkedin.com/in/thibaud-le-graverend/))._

## Planning détaillé

- [Lundi](./planning/1.lundi.md)
- [Mardi](./planning/2.mardi.md)
- [Mercredi](./planning/3.mercredi.md)
- [Jeudi](./planning/4.jeudi.md)
- [Vendredi](./planning/5.vendredi.md)

## Introduction (rappel)

[Ici.](./introduction.md)

## Remerciements

- [Scaleway](www.scaleway.com) pour la mise à disposition de crédits 🚀
