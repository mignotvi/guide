# Mercredi

Objectifs pédagogiques :

- [ ] Développer votre autonomie

## QCM2

!> **🕑 9h -> 10h**

## Traintements en streaming

!> **🕑 Toute la matinée 10h15 -> 13h & 14h -> 15h30**

```bash
sudo apt install time
```

Objectifs pédagogiques :

- [ ] Comprendre les avantages / inconvénients du traitement en streaming
- [ ] Découpler les notions de complexité en temps et en mémore
- [ ] Confrontation avec le CPU-Bound et découverte du multi-processing

### Questions

Les données de la BAN à télécharger sont les suivantes: https://api0057.s3.fr-par.scw.cloud/ban-splitted/ban_split.tar.gz

Il s'agit du fichier source séparé en plusieurs fichiers.

> [!EXERCISE]
> Faire un _générateur_ qui lit successivement chaque ligne de chaque fichier
>
> Indice:
>
> ```python
> from pathlib import Path
>
> for path in Path(...).rglob("*.csv"):
>    ...
> ```

_NB: vous pouvez obtenir la RAM maximale utilisée en prenant lançant votre code avec `/usr/bin/time poetry run pyhton ...`_

> [!EXERCISE] > **En streaming**
>
> - Calculer est la moyenne et l'écart-type de la distance de toutes les addresses à Compiègne
> - Trouver les 10 addresses les plus distantes de Compiègne

> [!TEACHER]
> Point sur CPU-Bound vs IO-Bound

> [!TEACHER]
>
> Point sur associatif et commutatif

> [!EXERCISE]
>
> Répéter la question précendente en parallélisant la lecture n fichiers en // à l'aide de multiprocessing

## Map reduce et Spark (partie 1)

_Nous vous avons préparé un dossier dans playgound pour travailler avec PySpark: [ici](https://gitlab.utc.fr/api0057/api0057-e2022/playground/-/tree/main/mercredi/spark_rdd)_

!> **🕑 Le reste de l'après-midi 15h30 -> 18h30**

Objectifs pédagogiques :

- [ ] Comprendre et appliquer le principe général de `Map/Reduce`
- [ ] Prendre en main `Spark` et `RDD`
- [ ] Savoir faire un tunnel SSH

> [!EXERCISE]
>
> (en RDD)
> Word count!
> [Fichier source](https://ocw.mit.edu/ans7870/6/6.006/s08/lecturenotes/files/t8.shakespeare.txt)

> [!EXERCISE]
>
> (en RDD)
> Trouver les 10 addresses les plus distantes de Compiègne

> [!EXERCISE]
>
> (en RDD)
> Calculer le centroid de chaque commune de France à partir des addresses de la BAN.

> [!BONUS]
>
> (en RDD)
> Réaliser un K-means sur les adresses
