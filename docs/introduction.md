## Introduction

Nous vous proposons une API pour découvrir (ou redécouvrir) le monde de l'_Applied Data Science_ au travers des thématiques de _Data-Engineering_ et de _Software-Engineering_ avec un fil rouge autour des donnés géographiques.

Quelques mots-clés pour vous donner envie ou attiser votre curiosité : Git, Linux, SSH, Python, tests, Structures de données (oui ça sert vraiment dans la vraie vie et pas que en NF16), Haute-Volumétrie, traitements en streaming, Map Reduce / Apache Spark, Dagster, sans oublier PostgreSQL et PostGIS.

**Il va de soi que vous ne serez jamais des experts de tout cela en 5 jours, mais il s'agit bien de vous donner des billes et des réflexes pour votre future vie professionnelle.**

## Objectifs pédagogiques

Objectif de l’Api : L'objet de cette Api est de fournir aux étudiants des connaissances et compétences de base dans la mise en œuvre du traitement de données, en particulier dans le cas de données massives.

Objectifs spécifiques :

- Savoir se repérer avec techniques modernes de traitement de données (Python, Spark, Streaming, SQL)
- Initiation au développement computationnellement efficace d'algos de traitements de données
- Découverte d'outils d'organisation de pipeline de calculs et d'observabilité

Objectifs transversaux :

- Mise en pratique de bonnes pratiques de dev (git, ssh, tests, etc.)
